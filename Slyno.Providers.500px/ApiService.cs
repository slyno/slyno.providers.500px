﻿namespace Slyno.Providers._500px
{
    using Newtonsoft.Json;
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class ApiService
    {
        private readonly Uri _baseUri;
        private readonly string _apiKey;

        public ApiService(string apiKey, string baseUrl = "https://api.500px.com/v1/photos/search")
        {
            _apiKey = apiKey;
            _baseUri = new Uri(baseUrl);
        }

        public async Task<SearchResult> Search(Query query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = _baseUri;

                var url = String.Format("?consumer_key={0}{1}", _apiKey, query);

                var response = await client.GetAsync(url);

                var json = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<SearchResult>(json);
            }
        }
    }
}
