﻿namespace Slyno.Providers._500px
{
    public class Category
    {
        private readonly string _name;
        
        private Category(string name)
        {
            _name = name;
        }

        public override string ToString()
        {
            return _name;
        }

        public static Category Abstract = new Category("Abstract");
        public static Category Animals = new Category("Animals");
        public static Category BlackAndWhite = new Category("Black and White");
        public static Category Celebrities = new Category("Celebrities");
        public static Category CityAndArchitecture = new Category("City & Architecture");
        public static Category Commercial = new Category("Commercial");
        public static Category Concert = new Category("Concert");
        public static Category Family = new Category("Family");
        public static Category Fashion = new Category("Fashion");
        public static Category Film = new Category("Film");
        public static Category FineArt = new Category("Fine Art");
        public static Category Food = new Category("Food");
        public static Category Journalism = new Category("Journalism");
        public static Category Landscapes = new Category("Landscapes");
        public static Category Macro = new Category("Macro");
        public static Category Nature = new Category("Nature");
        public static Category Nude = new Category("Nude");
        public static Category People = new Category("People");
        public static Category PerformingArts = new Category("Performing Arts");
        public static Category Sport = new Category("Sport");
        public static Category StillLife = new Category("Still Life");
        public static Category Street = new Category("Street");
        public static Category Transportation = new Category("Transportation");
        public static Category Travel = new Category("Travel");
        public static Category Underwater = new Category("Underwater");
        public static Category UrbanExploration = new Category("Urban Exploration");
        public static Category Wedding = new Category("Wedding");
    }
}
