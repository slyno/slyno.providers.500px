﻿namespace Slyno.Providers._500px
{
    public enum ImageSize
    {
        Small = 1,
        Medium = 2,
        Large = 3,
        ExtraLarge = 4
    }
}
