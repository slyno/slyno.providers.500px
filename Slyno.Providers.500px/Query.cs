﻿namespace Slyno.Providers._500px
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class Query
    {
        public Category Category { get; set; }

        private int _resultSize = 25;
        public int ResultSize
        {
            get { return _resultSize; }
            set { _resultSize = value; }
        }

        private ImageSize _imageSize = ImageSize.Large;
        public ImageSize ImageSize
        {
            get { return _imageSize; }
            set { _imageSize = value; }
        }

        private List<string> _tags = new List<string>();
        public List<string> Tags
        {
            get { return _tags; }
            set { _tags = value; }
        }

        private List<string> _keywords = new List<string>();
        public List<string> Keywords
        {
            get { return _keywords; }
            set { _keywords = value; }
        }

        private bool _includeTagsInResults = true;
        public bool IncludeTagsInResults
        {
            get { return _includeTagsInResults; }
            set { _includeTagsInResults = value; }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (this.Category != null)
            {
                sb.AppendFormat("&only={0}", Uri.EscapeUriString(this.Category.ToString()));
            }

            sb.AppendFormat("&image_size={0}", (int)this.ImageSize);

            if (this.Tags.Count > 0)
            {
                sb.AppendFormat("&tag={0}", String.Join("+", this.Tags.Select(Uri.EscapeUriString)));
            }

            if (this.Keywords.Count > 0)
            {
                sb.AppendFormat("&term={0}", String.Join("+", this.Tags.Select(Uri.EscapeUriString)));
            }

            if (this.ResultSize > 100)
            {
                this.ResultSize = 100;
            }
            if (this.ResultSize < 0)
            {
                this.ResultSize = 20;
            }

            sb.AppendFormat("&rpp={0}", this.ResultSize);

            if (this.IncludeTagsInResults)
            {
                sb.Append("&tags=true");
            }

            return sb.ToString();
        }
    }
}
