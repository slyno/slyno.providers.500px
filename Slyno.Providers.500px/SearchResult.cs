﻿namespace Slyno.Providers._500px
{
    using System.Collections.Generic;

    public class SearchResult
    {
        public int current_page { get; set; }

        public int total_pages { get; set; }

        public int total_items { get; set; }

        public List<Photo> photos { get; set; }
    }
}
